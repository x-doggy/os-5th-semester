#include "../apue.h"
#include <pthread.h>

#define MSG_LENGTH     255
#define QUIT          "quit"
#define CLIENTS_COUNT  8              /* кол-во клиентов */
#define MSG_FILE      "messages.txt"  /* имя файла */

pthread_rwlock_t buf_lock;
pthread_rwlock_t file_lock;
pthread_rwlock_t var_lock;
pthread_rwlock_t msg_lock;
pthread_rwlock_t done_lock;

int done,                      /* Признак завершения работы (устанавливает сервер) */
    working_clients,           /* Число клиентов, которые должны записать в файл */
    message_num;               /* Номер сообщения */
char message_buf[MSG_LENGTH];  /* Буфер под сообщение */

FILE *file;                    /* Файл куда пишут клиенты */

void *server(void *argp) {

    puts("Server working");
    puts("Write you message and press [ENTER]");
    puts("Write \"" QUIT "\" to stop the server");

    while (1) {
        if (working_clients) continue;
        /*
         * Цикл не пойдет дальше, пока working_clients не уменьшится до нуля.
         * Каждый клиент уменьшает эту переменную, как сделает своё дело.
         * Отсюда сервер знает, что ВСЕ клиенты отработали и можно писать в файл следующее сообщение.
         */

        pthread_rwlock_wrlock(&buf_lock);
        scanf("%s", message_buf);

        if (strcmp(message_buf, QUIT) == 0) {
            pthread_rwlock_unlock(&buf_lock);
            puts("Server done");
            pthread_rwlock_wrlock(&done_lock);
            done = 1;
            pthread_rwlock_unlock(&done_lock);
            return 0;
        }

        pthread_rwlock_unlock(&buf_lock);

        pthread_rwlock_wrlock(&msg_lock);
        message_num++;
        pthread_rwlock_unlock(&msg_lock);

        pthread_rwlock_wrlock(&var_lock);
        working_clients = CLIENTS_COUNT;
        pthread_rwlock_unlock(&var_lock);

        sched_yield();
    }

    return 0;
}

void *client(void *argp) {

    /* Номер клиента-читателя */
    int cid = *(int *) argp;
    /* Номер сообщения, которое нужно этому читателю */
    int curr_message = 1;
    /* Фактический номер сообщения (аналог message_num) */
    int client_message_num = 0;
    /* Признак выхода (аналог done) */
    int need_exit = 0;
    /* Сообщение клиента (аналог message_buf) */
    char client_message[MSG_LENGTH];

    printf("Client %d working\n", cid);

    while (1) {
        pthread_rwlock_rdlock(&done_lock);
        need_exit = done;
        pthread_rwlock_unlock(&done_lock);

        if (need_exit) {
            /*
             * Увидели, что сервер отключился, завершаем свою работу.
             */
            printf("Client %d done!\n", cid);
            return 0;
        }

        pthread_rwlock_rdlock(&msg_lock);
        client_message_num = message_num;
        pthread_rwlock_unlock(&msg_lock);

        if (curr_message == client_message_num) {
            pthread_rwlock_rdlock(&buf_lock);
            strcpy(client_message, message_buf);
            pthread_rwlock_unlock(&buf_lock);

            pthread_rwlock_wrlock(&file_lock);

            file = fopen(MSG_FILE, "a+");
            fprintf(file, "Client %d has received \"%s\"\n", cid, client_message);
            fclose(file);
            curr_message++;

            pthread_rwlock_unlock(&file_lock);

            pthread_rwlock_wrlock(&var_lock);
            working_clients--;
            pthread_rwlock_unlock(&var_lock);
        }

        sched_yield();
    }

    free(argp);
    return 0;
}

int main(int argc, char **argv) {

    pthread_t server_thread;                // дескриптор потока сервера
    pthread_t client_thread[CLIENTS_COUNT]; // дескрипторы клиентских потоков

    if (pthread_rwlock_init(&buf_lock, 0)) {
        err_sys("Cannot init blocking buf");
    }
    if (pthread_rwlock_init(&file_lock, 0)) {
        err_sys("Cannot init blocking file");
    }
    if (pthread_rwlock_init(&var_lock, 0)) {
        err_sys("Cannot init blocking var");
    }
    if (pthread_rwlock_init(&msg_lock, 0)) {
        err_sys("Cannot init blocking message");
    }
    if (pthread_rwlock_init(&done_lock, 0)) {
        err_sys("Cannot init blocking done");
    }

    if (pthread_create(&server_thread, (void *) NULL, server, (void *) NULL)) {
        err_sys("Cannot create server thread");
    }

    for (int i=0; i<CLIENTS_COUNT; i++) {
        int *q = (int *) malloc( sizeof(int) );
        *q = i;
        if (pthread_create(&client_thread[i], NULL, client, (void *) q)) {
            err_sys("Cannot create client thread");
        }
    }

    for (int i=0; i<CLIENTS_COUNT; i++) {
        pthread_join(client_thread[i], 0);
    }

    pthread_join(server_thread, 0);

    if (pthread_rwlock_destroy(&buf_lock)) {
        err_sys("Cannot destroy blocking buf");
    }
    if (pthread_rwlock_destroy(&file_lock)) {
        err_sys("Cannot destroy blocking file");
    }
    if (pthread_rwlock_destroy(&var_lock)) {
        err_sys("Cannot destroy blocking var");
    }
    if (pthread_rwlock_destroy(&msg_lock)) {
        err_sys("Cannot destroy blocking message");
    }
    if (pthread_rwlock_destroy(&done_lock)) {
        err_sys("Cannot destroy blocking done");
    }

    return 0;
}
