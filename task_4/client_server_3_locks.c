#include "../apue.h"
#include <pthread.h>

#define MSG_LENGTH     255            /* Макс. длина строки */
#define CLIENTS_COUNT  10             /* Кол-во клиентов */
#define MSG_FILE      "messages.txt"  /* Имя файла */
#define QUIT          "quit"          /* Сообщение о выходе */

pthread_rwlock_t file_lock;
pthread_rwlock_t buffer_lock;
pthread_rwlock_t done_lock;

int done;                      /* Признак завершения работы (устанавливает сервер) */
FILE *f;                       /* Файл куда пишут клиенты */
char message_buf[MSG_LENGTH];  /* Буфер под сообщение */


void *server(void *arg) {

    char local_message_buf[MSG_LENGTH];
    int  local_done;

    while (1) {

        pthread_rwlock_rdlock(&done_lock);
        local_done = done;
        pthread_rwlock_unlock(&done_lock);

        if (local_done) continue;

        fgets(local_message_buf, sizeof(local_message_buf), stdin);
        local_message_buf[strcspn(local_message_buf, "\n")] = 0;

        pthread_rwlock_wrlock(&buffer_lock);
        strcpy(message_buf, local_message_buf);
        pthread_rwlock_unlock(&buffer_lock);

        if (strcmp(local_message_buf, QUIT) == 0){
            puts("Server shutdown!");
            pthread_rwlock_wrlock(&done_lock);
            done = 1;
            pthread_rwlock_unlock(&done_lock);
            break;
        }

        pthread_rwlock_wrlock(&done_lock);
        done = 1;
        pthread_rwlock_unlock(&done_lock);

        sched_yield();
    }

    return (void *) 1;
}


void *client(void *arg) {

    char local_message_buf[MSG_LENGTH];
    int local_done;

    while (1) {

        pthread_rwlock_rdlock(&done_lock);
        local_done = done;
        pthread_rwlock_unlock(&done_lock);

        if (!local_done) continue;
        /* Цикл не пойдет дальше, пока сервер не разрешит работу done */

        /*
         * Использование локальных копий глобальных переменных уменьшает
         * время ожидания других потоков при их использовании.
         */
        pthread_rwlock_wrlock(&buffer_lock);
        strcpy(local_message_buf, message_buf);
        pthread_rwlock_unlock(&buffer_lock);

        if (strcmp(local_message_buf, QUIT) == 0){
            puts("Client shutdown!");
            break;
        }

        pthread_rwlock_wrlock(&file_lock);
        f = fopen(MSG_FILE, "a+");
        fprintf(f, "%s\n", local_message_buf);
        fclose(f);
        pthread_rwlock_unlock(&file_lock);

        pthread_rwlock_wrlock(&done_lock);
        done = 0;
        pthread_rwlock_unlock(&done_lock);

        sched_yield();
    }

    free(arg);
    return (void *) 1;
}


int main(int argc, char **argv) {

    /* Инициализируем блокировки */
    if (pthread_rwlock_init(&file_lock, NULL)) {
        err_sys("Cannot init rwlock...");
    }
    puts("file_lock successfully inited!");

    if (pthread_rwlock_init(&buffer_lock, NULL)) {
        err_sys("Cannot init rwlock...");
    }
    puts("buffer_lock successfully inited!");

    if (pthread_rwlock_init(&done_lock, NULL)) {
        err_sys("Cannot init rwlock...");
    }
    puts("done_lock successfully inited!");


    /* Создаём нить-сервер */
    pthread_t server_thread;

    if (pthread_create(&server_thread, NULL, server,  NULL)) {
        err_sys("Cannot create server thread...");
    }
    puts("Server successfully created!");


    /* Создаём нити-клиенты */
    pthread_t clients[CLIENTS_COUNT];

    for (int i = 0; i < CLIENTS_COUNT; i++) {
        /* Использование кучи уменьшает конкурентность нитей */
        int *q = (int *) malloc( sizeof(int) );
        *q = i;

        if (pthread_create(&clients[i], NULL, client, (void *) q)) {
            err_sys("Cannot create client[%d] thread...\n", i);
        }
        printf("Client [%d] created!\n", i);
    }


    /* Ждём выполнения всех нитей */
    for (int i = 0; i < CLIENTS_COUNT; i++) {
        pthread_join(clients[i], NULL);
    }

    pthread_join(server_thread, NULL);


    /* Уничтожаем блокировки */
    if (pthread_rwlock_destroy(&done_lock)) {
        err_sys("Cannot destroy rwlock...");
    }
    puts("done_lock successfully destroyed!");

    if (pthread_rwlock_destroy(&buffer_lock)) {
        err_sys("Cannot destroy rwlock...");
    }
    puts("buffer_lock successfully destroyed!");

    if (pthread_rwlock_destroy(&file_lock)) {
        err_sys("Cannot destroy rwlock...");
    }
    puts("file_lock successfully destroyed!");

    return 0;
}
