#include "../apue.h"

int main(int argc, char **argv) {

    int bufsize;
    if (argc != 2 || (bufsize = atoi(argv[1])) == 0) {
        err_sys("Usage: mycat <buffsize>");
    }

    int n;
    char buf[bufsize];

    while ((n = read(0, buf, bufsize)) > 0) { // 0 == stdin
        printf("Buf size = %d\n", n);
        if (write(1, buf, n) != n)          // 1 == stdout
            err_sys("write error");
    }

    if (n < 0)
        err_sys("read error");

    return 0;
}

