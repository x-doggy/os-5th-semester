#!/bin/bash

check_utils() {
  local needed_prgs=(
    "cat"
    "dd"
    "/usr/bin/time"
    "grep"
    "sed"
    "truncate"
    "bash"
    "bc"
    "gnuplot"
  )
  local ok=1
  for i in "${needed_prgs[@]}" ; do
    echo "Check if $i exists..."
    if [ -x `command -v $i` ] ; then
      echo "[OK]        $i"
    else
      echo "[NOT FOUND] $i"
      ok=0
    fi
  done
  if [[ $ok -eq 0 ]] ; then
    echo "Please install necessary dependencies before continue" 1>&2
    exit 1
  fi
}

echo "+--------------------------------+"
echo "|   S C R I P T    S T A R T S   |"
echo "+--------------------------------+"

check_utils

printf -v FILE 'file%(%s)T' -1
# FILE=file + current time since the Epoch

DD_BS=4096
DD_COUNT=51200
BUFSIZE=512
MAXBUFSIZE=4194304

truncate -s 0 log.txt

#if [ ! -e mycat ] ; then
#    gcc -Wall task_2_2.c -o mycat
#fi

while [ $BUFSIZE -lt $((MAXBUFSIZE+1)) ] ; do
    dd if=/dev/urandom of=$FILE.in bs=$DD_BS count=$DD_COUNT
    #/usr/bin/time -o log.txt -a -p ./mycat $BUFSIZE < $FILE.in > $FILE.out
    a=$(echo $( TIMEFORMAT="%3U + %3S"; { time ./mycat $BUFSIZE < $FILE.in > $FILE.out; } 2>&1) "*100" | sed 's/,/./g' | bc -l)
    echo -e "$BUFSIZE $a" >> log.txt
    BUFSIZE=$((BUFSIZE * 2))
    rm -f $FILE*
done

echo "Making plot using gnuplot..."
gnuplot plotscript.gp

echo "+----------------------------+"
echo "|   S C R I P T    E N D S   |"
echo "+----------------------------+"

if [ -s log.txt ]; then
    cat log.txt
fi
