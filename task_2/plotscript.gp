#!/usr/bin/gnuplot -persist

set title "Зависимость времени копирования файла от размера буфера.\nВыполнено в Arch Linux на Intel Core i7 3610 QM"
set terminal png size 1000,1000
set size square
set logscale x
set logscale y 2
set ytics scale 2
set xlabel "Буфер"
set ylabel "Время"
set style line 1 linecolor rgb '#dd181f' \
    linetype 1 linewidth 2 \
    pointtype 5 pointsize 1.5
set output 'plot.png'

plot 'log.txt' using 1:2:xticlabels(1):yticlabels(2) title "Зависимость" with linespoints linestyle 1
