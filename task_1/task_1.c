#include "../apue.h"
#include <fcntl.h>

char buf1[] = "abcdefghij";
char buf2[] = "ABCDEFGHIJ";

int main(int argc, char **argv) {

    int fd;

    if ((fd = creat("file.hole", FILE_MODE)) < 0)
        err_sys("error calling creat");

    if (write(fd, buf1, 10) != 10)
        err_sys("error writing buf1");
    // теперь текущая позиция = 10

    if (lseek(fd, 16384, SEEK_SET) == -1)
        err_sys("error calling lseek");
    // теперь текущая позиция = 16384

    if (write(fd, buf2, 10) != 10)
        err_sys("error writing buf2");
    // теперь текущая позиция = 16394

    return 0;
}
