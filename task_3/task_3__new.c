#include "../apue.h"
#include <dirent.h>

#define STR_MAX 254

int count_symlinks(char *cur_path, char *path, int node, dev_t main_uid, int is_root) {

    int links = 0;
    int is_head = 0;
    
    const char dot[]   = "/..";
    const char slash[] = "/";
    
    struct stat    st;
    struct dirent *dp;
    
    dev_t  cur_uid;
    DIR   *dir = opendir(path);

    while (1) {
        char full_path[STR_MAX];

        dp = readdir(dir);     // read node from directory
        if (!dp) {
            chdir("..");
            break;
        }

        ino_t cur_node = 0;

        snprintf(full_path, (sizeof full_path) + 3, "%s%s%s", path, slash, dp->d_name);

        lstat(full_path, &st); // информация о симв. ссылке
        cur_node = st.st_ino;  // текущий индексный узел
        cur_uid = st.st_dev;   // текущий идентификатор устройства

        if (cur_node == 2 && cur_uid == main_uid) {
            is_head = 1;
	      }

        puts(full_path);

        puts(dp->d_name);

        
        if (0 == strcmp(dp->d_name, cur_path)) {
          links = 1;
        }

//        if (S_ISLNK(st.st_mode)) { // симв. ссылка?
//            char lnk_path[STR_MAX];
//            int len = readlink(full_path, lnk_path, STR_MAX); // считываем зн-ие симв. ссылки
//            lnk_path[len] = 0;
//
//            lstat(lnk_path, &st);
//            if (st.st_ino == node) {
//                links++;
//                printf("LINKSLYMB = %s\n", lnk_path);
//            }
//        }
    }

    puts("\n\n");

    if (is_head) {
        if (is_root) return links;
        is_root = 1;
    }

    strcat(path, dot);
    links += count_symlinks(cur_path, path, node, main_uid, is_root);

    return links;
}

int main(int argc, char **argv) {

    char path_is_header[100] = "/";
    char path[STR_MAX] = {0};

    getcwd(path, STR_MAX);
    printf("Path = %s\n", path);

    struct stat st;
    
    lstat(path_is_header, &st);    // получаем информацию из индексного узла
    dev_t main_uid = st.st_dev;    // идентификатор устройства, на котором нах. инд. узел
    printf("Main ID = %lu\n", main_uid);

    lstat(path, &st);
    ino_t node = st.st_ino;        // inode number
    printf("Node = %lu\n\n\n", node);
    
    // получаем базовое название исходного каталога
    DIR   *dir = opendir(path);
    struct dirent *dp;
    dp = readdir(dir);

    printf("Number of symlinks = %d\n\n", count_symlinks(dp->d_name, path, node, main_uid, 0));

    return 0;
}
