#include "source.h"

int main(int argc, char **argv) {

    char *file_name = argv[1];
    pid_t client_pid;
    ssize_t data_read_number;
    int server_fifo_fd, client_fifo_fd;
    char client_fifo_name[CLIENT_NAME_SIZE];
    char some_data[BUFFER_SIZE];
    char str_from_file[STRING_SIZE];

    // Making a FIFO
    client_pid = getpid();
    sprintf(client_fifo_name, CLIENT_FIFO_NAME, client_pid);
    unlink(client_fifo_name);
    if (mkfifo(client_fifo_name, 0777) < 0) {
        err_quit("FIFO < %s > not initiated\n", client_fifo_name);
    }
    printf("FIFO < %s > inited!\n", client_fifo_name);
    puts("-----------------------------------------");

    if ((server_fifo_fd = open(SERVER_FIFO_NAME, O_WRONLY)) < 0)  {
        err_sys("Error opening for write");
    }

    int fd;
    if ((fd = open(file_name, O_RDONLY)) < 0) {
        err_sys("Error file open");
    }

    while (1) {
        data_read_number = read(fd, str_from_file, STRING_SIZE);
        if (data_read_number < 0) {
            err_sys("Error reading from file");
        }
        if (!data_read_number) {
            //клиент получил признак конца файла
            printf("Client %d done!\n", client_pid);
            puts("-----------------------------------------");
            break;
        }

        snprintf(some_data, BUFFER_SIZE, str_from_file, client_pid);
        // строки в файле должны быть записаны в формате "%d:текст"

        if (write(server_fifo_fd, some_data, data_read_number + 1) < 0) {
            err_sys("Error writing into buffer");
        }
        printf("Sent: %s", some_data);
        if ((client_fifo_fd = open(client_fifo_name, O_RDONLY)) < 0) {
            err_quit("Error opening FIFO of %d", client_pid);
        }
        if (read(client_fifo_fd, some_data, BUFFER_SIZE) < 0) {
            err_quit("Error reading from FIFO %d", client_pid);
        }
        printf("Received: %s\n", some_data);
        if (close(client_fifo_fd) < 0) {
            err_quit("Error closing FIFO %d", client_pid);
        }
    }
    if (close(fd) < 0) {
        err_sys("Error closing file");
    }
    if (unlink(client_fifo_name) < 0) {
        err_quit("Error deleting FIFO %d", client_pid);
    }
    if (close(server_fifo_fd) < 0) {
        err_sys("Error closing server FIFO");
    }

    return 0;
}

