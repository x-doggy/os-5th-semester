#!/bin/bash

NUM=10
CNT=0
FILENAME="example.txt"

if [ ! -f $FILENAME ] ; then
    #   Uncomment this and comment following lines until 'fi'
    #   if you want exit when file doesn't exists
    # echo "$FILENAME doesn\'t exists!"
    # exit 1
    touch $FILENAME
    echo -n "Please, enter the message: "
    read line
    echo "%d:$line" > $FILENAME
    if [ -s $FILENAME ] ; then
        echo "File $FILENAME was successfully written!"
    else
        echo "Something\'s goes wrong..."
    fi
fi

sleep 1

echo "Launching server..."
./server
echo "Server launched!"

sleep 1

CNT=0
while [ $CNT -lt $NUM ] ; do
    echo "Launching client $CNT..."
    ./client $FILENAME &
    CNT=$((CNT+1))
done

echo "Killing both server and clients..."
killall server client
