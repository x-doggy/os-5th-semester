#include "../apue.h"
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <syslog.h>
#include <ctype.h>
#include <time.h>

#define SERVER_FIFO_NAME "/tmp/server_fifo"
#define CLIENT_FIFO_NAME "/tmp/client_%d_fifo"
#define STRING_SIZE 32
#define BUFFER_SIZE 32
#define ID_SIZE 8
#define CLIENT_NAME_SIZE 128

void clr_fl(int fd, int flags) {

    int val;

    if ((val = fcntl(fd, F_GETFL, 0)) == -1) {
        err_sys("fcntl F_GETFL error");
    }

    val &= ~flags;

    if (fcntl(fd, F_SETFL, val) == -1) {
        err_sys("fcntl F_SETFL error");
    }
}
