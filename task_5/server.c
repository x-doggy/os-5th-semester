#include "source.h"

int main(int argc, char **argv) {

    pid_t pid;
    if ((pid = fork()) < 0 ) {
        err_sys("Error calling fork");
    }
    else if (pid != 0) {
        exit(0);
    }

    if (setsid() < 0) {
        err_sys("Error calling setsid");
    }
    if (chdir("/") < 0) {
        err_sys("Unable to change root /");
    }

    // Закрыть все открытые файловые дескрипторы
    int opened_fd;
    opened_fd = sysconf(_SC_OPEN_MAX);
    int i;
    for (i = opened_fd - 1; i >= 0; --i) {
        close(i);
    }

    signal(SIGPIPE, SIG_IGN);
    umask(0);

    // Присоединить файловые дескрипторы 0, 1 и 2 к /dev/null
    open("/dev/null", O_RDONLY);
    open("/dev/null", O_WRONLY);
    dup(1);

    // Инициализировать файл журнала
    openlog("FIFO ", LOG_PID | LOG_CONS | LOG_NDELAY | LOG_NOWAIT, LOG_LOCAL0);

    int client_pid;
    int server_fifo_fd, fdwrite;
    int client_fifo_fd;
    ssize_t data_read_number;
    char buf[BUFFER_SIZE];
    char client_fifo_name[CLIENT_NAME_SIZE];

    unlink(SERVER_FIFO_NAME);

    if (mkfifo(SERVER_FIFO_NAME, 0777) < 0) {
        syslog(LOG_LOCAL0 | LOG_ERR, "Error calling mkfifo");
        exit(1);
    }
    if ((server_fifo_fd = open(SERVER_FIFO_NAME, O_RDONLY | O_NONBLOCK)) < 0) {
        syslog(LOG_LOCAL0 | LOG_ERR, "Error open for reading %s", SERVER_FIFO_NAME);
        exit(1);
    }
    if ((fdwrite = open(SERVER_FIFO_NAME, O_WRONLY)) < 0) {
        syslog(LOG_LOCAL0 | LOG_ERR, "Error open for writing %s", SERVER_FIFO_NAME);
        exit(1);
    }

    clr_fl(server_fifo_fd, O_NONBLOCK);

    // ждем поступления сигнала
    while (1) {
        data_read_number = read(server_fifo_fd, buf, BUFFER_SIZE);
        if (data_read_number < 0) {
            syslog(LOG_LOCAL0 | LOG_ERR, "Reading error");
            exit(1);
        }
        int counter = 0;
        char temp[ID_SIZE];
        while (buf[counter] != ':') {
            temp[counter] = buf[counter];
            counter++;
        }
        temp[counter] = '\0';
        client_pid = atoi(temp);
        sprintf(client_fifo_name, CLIENT_FIFO_NAME, client_pid);
        if ((client_fifo_fd = open(client_fifo_name, O_WRONLY)) < 0) {
            syslog(LOG_LOCAL0 | LOG_ERR, "Error opening %d", client_fifo_fd);
            exit(1);
        }
        while (buf[counter]) {
            if (buf[counter] >= 'a' && buf[counter] <= 'z')
                buf[counter] = (char) toupper(buf[counter]);
            counter++;
        }

        ssize_t error_write = write(client_fifo_fd, buf, counter);
        if (errno == EPIPE) {
            syslog(LOG_LOCAL0 | LOG_ERR, "SIGPIPE received. Client %d don't read his FIFO", client_fifo_fd);
            exit(1);
        }
        else if (error_write < 0) {
            syslog(LOG_LOCAL0 | LOG_ERR, "Error writing into %d", client_fifo_fd);
            exit(1);
        }
        if (close(client_fifo_fd) < 0) {
            syslog(LOG_LOCAL0 | LOG_ERR, "Error closing %d", client_fifo_fd);
            exit(1);
        }
    }
}
